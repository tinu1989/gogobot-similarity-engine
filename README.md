# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Install Required Libraries

pip install pandas
pip install numpy
pip install scikit-learn
pip install nltk
pip install tqdm
pip install scipy
pip install python-levenshtein
pip install --upgrade gensim
pip install Django==1.9.4
pip install djangorestframework
pip install markdown       
pip install django-filter
pip install django-cors-headers
pip install jsonfield

2.Download Required Language libraries


sudo python -m nltk.downloader stopwords

2.Run

python manage.py runserver IP
