'''
Created on Jul 12, 2017

@author: aneesh.c
'''
import gensim
from config import *
import numpy as np
from tqdm import tqdm
from nltk import word_tokenize
from sklearn.preprocessing import MinMaxScaler
from scipy.spatial.distance import cosine, cityblock, jaccard, canberra, euclidean, minkowski, braycurtis


from nltk.corpus import stopwords
stop_words = stopwords.words('english')

scalar = MinMaxScaler()

model = gensim.models.KeyedVectors.load_word2vec_format('similariy_score_computation/similariy_score_computation/similariy_score_computation/model.bin', 
                                                        binary=True)

def sent2vec(s):
    words = word_tokenize(s)
    words = [w for w in words if not w in stop_words]
    words = [w for w in words if w.isalpha()]
    M = []
    for w in words:
        try:
            M.append(model[w])
        except Exception as e:
            print e
            pass

    M = np.array(M)

    v = M.sum(axis=0)

    return v / np.sqrt((v ** 2).sum())


def compute_score(user_question):
    table = get_table()

    table['question2'] = user_question
    question1_vectors = np.zeros((table.shape[0], 300))
    for i, q in enumerate(table.question.values):
        question1_vectors[i, :] = sent2vec(q)
    question2_vectors = np.zeros((table.shape[0], 300))
    for i, q in enumerate(table.question2.values):
        question2_vectors[i, :] = sent2vec(q)
    table['cosine_distance'] = [cosine(x, y) for (x, y) in zip(np.nan_to_num(question1_vectors),
                                                              np.nan_to_num(question2_vectors))]
    table['cityblock_distance'] = [cityblock(x, y) for (x, y) in zip(np.nan_to_num(question1_vectors),
                                                              np.nan_to_num(question2_vectors))]
    table['canberra_distance'] = [canberra(x, y) for (x, y) in zip(np.nan_to_num(question1_vectors),
                                                              np.nan_to_num(question2_vectors))]
    table['euclidean_distance'] = [euclidean(x, y) for (x, y) in zip(np.nan_to_num(question1_vectors),
                                                              np.nan_to_num(question2_vectors))]
    table['minkowski_distance'] = [minkowski(x, y, 3) for (x, y) in zip(np.nan_to_num(question1_vectors),
                                                              np.nan_to_num(question2_vectors))]
    table['braycurtis_distance'] = [braycurtis(x, y) for (x, y) in zip(np.nan_to_num(question1_vectors),
                                                              np.nan_to_num(question2_vectors))]
    table = table.dropna()
    table_subset = table[['cosine_distance','cityblock_distance',
    'canberra_distance','euclidean_distance','minkowski_distance','braycurtis_distance']]

    table_subset = scalar.fit_transform(table_subset)
    table['sum'] = table_subset.sum(axis=1)
    table = table.sort_values(['sum'], ascending=[True])
    json_list = table.to_dict(orient='records')
    json = json_list[0]
    return json