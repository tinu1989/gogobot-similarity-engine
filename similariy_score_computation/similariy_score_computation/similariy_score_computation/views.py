'''
Created on Jul 12, 2017

@author: aneesh.c
'''
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework import permissions
import random
import dill
import base64
from compute_similarity import compute_score

@permission_classes((permissions.AllowAny,))
class TestAPI(viewsets.ViewSet):
    def create(self, request):
        input = request.data
        print 'hi'
        question = input['user_input']
        json = compute_score(question)
        return Response(json)